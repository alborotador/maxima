#1.Задача:
#В купейном вагоне имеется 9 купе с четырьмя местами для пассажиров в каждом.
#Напишите программу, которая определяет номер купе, в котором находится место
#с заданным номером (нумерация мест сквозная, начинается с 1).

#1 вариант:
num = int(input("Enter the seat number: "))
if 0 < num <= 4:
    print("Your compartment number 1.")
elif 4 < num <= 8:
    print("Your compartment number 2.")
elif 8 < num <= 12:
    print("Your compartment number 3.")
elif 12 < num <= 16:
    print("Your compartment number 4.")
elif 16 < num <= 20:
    print("Your compartment number 5.")
elif 20 < num <= 24:
    print("Your compartment number 6.")
elif 24 < num <= 28:
    print("Your compartment number 7.")
elif 28 < num <= 32:
    print("Your compartment number 8.")
elif 32 < num <= 36:
    print("Your compartment number 9.")
else:
    print("Location is incorrect!")

#2 вариант:
num = int(input("Enter the seat number: "))
if ((num-1) // 4 + 1):
    print(f"Your compartment number {num}.")
else:
    print("Location is incorrect!")